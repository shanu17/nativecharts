# A Project with CI & CD .......
# <br>
# Better Optimization
## by adding PureComponent <br>
## Cache the images locally. ...<br>
## Avoiding Unnecessary Renders<br>
# <br>
# Added UI for Login & SignUp with 2FA

<table>
  <tr>
     <td>Menu Screen</td>
     <td>Login Screen</td>
     <td>Input value is wrong</td>
  </tr>
  <tr>
    <td><img src="https://drive.google.com/uc?export=view&id=12KVMVObn4tIoocn8f3dav2-oN-1dbYrc" width=270 height=480></td>
    <td><img src="https://drive.google.com/uc?export=view&id=1xaHeKM4S5G_HGHGvNilH5BGFWbJGIf3a" width=270 height=480></td>
    <td><img src="https://drive.google.com/uc?export=view&id=1tZu5dNAx0fqH8ALpf5_aat1df72S1BFG" width=270 height=480></td>
  </tr>
 </table>


<table>
  <tr>
     <td>SignUp Screen</td>
     <td>Forgot Password Screen</td>
     <td>Main Screen</td>
  </tr>
  <tr>
    <td><img src="https://drive.google.com/uc?export=view&id=11VrLE-3lfM1LPmgr47fF3FSVDQHLw8uy" width=270 height=480 ></td>
    <td><img src="https://drive.google.com/uc?export=view&id=1zhlwf6AZh6YpLGl7bavInXD0epOAZhjh" width=270 height=480 ></td>
    <td><img src="https://drive.google.com/uc?export=view&id=1JOKCeGFxCiutQvzygo18Y9vT9L3dvvw7" width=270 height=480 ></td>
  </tr>
 </table>


<table>
<tr>
   <td>Drawer Screen</td>
   <td>HighCharts Screen</td>
   <td>ChartsKit Screen</td>
</tr>
<tr> 
  <td><img src="https://drive.google.com/uc?export=view&id=105qAW7bsDSQQqH_KOOdpIkOWt60TorHy" width=270 height=480 hspace="20"></td>
  <td><img src="https://drive.google.com/uc?export=view&id=1-qfMR3a2yfrgO-Adb2XaZd7ZRJTIYniz" width=270 height=480 hspace="20"></td>
  <td><img src="https://drive.google.com/uc?export=view&id=1-ln5HcNE4_CTY4gNYxUZFWhPofPwdAEz" width=270 height=480 hspace="20"></td>
</tr>
</table>

 <table>
<tr>
   <td>ChartsWrapper Screen</td>
</tr>
<tr> 
  <td><img src="https://drive.google.com/uc?export=view&id=1-wLALSI-WZQjuYzRIjFxMZSV-8yJLWAR" width=270 height=480 hspace="20"></td>
</tr>
</table>


