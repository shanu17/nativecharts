import React from 'react';
import {
    Image,
    FlatList,
    View, ActivityIndicator,
    Text,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
//  git remote add origin https://gitlab.com/shanu17/charts.git

class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            page: 1,
            isLoading: false,
            showDataa: false
        }
    }

    componentDidMount() {
        this.setState({ isLoading: true }, this.getData)
    }  
 
    getData = async () => {
        const URL = 'http://jsonplaceholder.typicode.com/photos'
        fetch(URL)
            .then((res => res.json()))
            .then((res) => {
                // console.log(res)
                this.setState({
                    // data: this.state.data.concat(res),
                    isLoading: false
                })
                this._storeData(res)
            })
    }

    _storeData = async (data) => {
        try {
            await AsyncStorage.setItem(
                'saved',
                JSON.stringify(data)
                //data.toString()
            );
        } catch (error) {
            // Error saving data
        }
    };

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('saved');
            if (value !== null) {
                // We have data!!
                this.setState({ data: JSON.parse(value), showDataa: true })
                console.log(JSON.parse(value));

            }
        } catch (error) {
            // Error retrieving data
        }
    };

    renderRow = ({ item }) => {
        console.log(item)
        return (
            <View style={{ flex: 1, borderBottomColor: '#ccc', marginBottom: 10, borderBottomWidth: 1, maxWidth: '50%' }}>
                <Image style={{ width: '100%', height: 200, resizeMode: 'cover' }} source={{ uri: item.url }} />
                <Text style={{ fontSize: 16, padding: 5 }}>{item.title}</Text>
                <Text style={{ fontSize: 16, padding: 5 }}>{item.id}</Text>
            </View>
        )
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1, isLoading: true
        }, this.getData)

    }

    renderFooter = () => {
        return (
            this.state.isLoading ?
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#00ff00" />
                </View> : null
        )
    }

    render() {

        // if (!this.state.data) {

        //     <Text onPress={this._retrieveData}>showing.....</Text>
        // }
        return (
            <View>

                {this.state.showDataa ? <FlatList
                    numColumns={2}
                    style={{ marginTop: 20, }}
                    data={this.state.data}
                    renderItem={this.renderRow}
                    keyExtractor={(item, index) => index.toString()}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={10}
                    ListFooterComponent={this.renderFooter}
                /> : null
                }

                <Text onPress={this._retrieveData}>showing.....</Text>
            </View>

        );
    }
};



export default App;
