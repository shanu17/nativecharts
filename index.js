/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import Saved from './SaveData'
import Context from './Context'
//import Chart from './src/screens/DrawerScreen/Charts'
//import ChartKit from './ChartsKit'
//import ChartsWrapper from './src/screens/DrawerScreen/ChartsWrapper'
import { gestureHandlerRootHOC } from 'react-native-gesture-handler'
import AppMain from './AppMain'

import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => gestureHandlerRootHOC(AppMain));

// busigence321

// const URL = `/beers?page=${page}&per_page=10`;
//   axiosService
//     .request({
//       url: URL,
//       method: 'GET'
//     })
// fetch('https://api.punkapi.com/v2' + URL)

/**
 * Adding new file
 * git status
 *  << all undone file will be in red >>
 * git add .\TestFile.js  <<< if want to add all then git add -A >>>
 * git status << done file will be in green >
 * git commit -m "Adding Test file"
 * git push origin master
 *
 * update a file
 *  go to git branch in VS CODE
 *  update file_name
 *  click tick and then sync below
 */