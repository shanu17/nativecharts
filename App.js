import React from 'react';
import {
  Image,
  FlatList,
  View, ActivityIndicator,
  Text,
} from 'react-native';
//  git remote add origin https://gitlab.com/shanu17/nativecharts.git
import ChildContext from './Context'
 
import Context from './AuthContext' 
 
class App extends React.PureComponent {
 
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      page: 1,
      isLoading: false  
    }
  }

  componentDidMount() {
    this.setState({ isLoading: true }, this.getData)
  }

  getData = async () => {
    const URL = 'http://jsonplaceholder.typicode.com/photos?_limit=10&_page=' + this.state.page
    fetch(URL).then((res => res.json()))
      .then((res) => {
        this.setState({
          data: this.state.data.concat(res),
          isLoading: false
        })
      })
  }

  renderRow = ({ item }) => {
    return (
      <View style={{ flex: 1, borderBottomColor: '#ccc', marginBottom: 10, borderBottomWidth: 1, maxWidth: '50%' }}>
        <Image style={{ width: '100%', height: 200, resizeMode: 'cover' }} source={{ uri: item.url }} />
        <Text style={{ fontSize: 16, padding: 5 }}>{item.title}</Text>
        <Text style={{ fontSize: 16, padding: 5 }}>{item.id}</Text>
      </View>
    )
  }

  handleLoadMore = () => {
    this.setState({
      page: this.state.page + 1, isLoading: true
    }, this.getData)
  }

  renderFooter = () => {
    return (
      this.state.isLoading ?
        <View style={{ marginTop: 10, alignItems: 'center' }}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View> : null
    )
  }

  render() {
    return (
      <Context.Provider value={{
        originalData: this.state.data,
        renItem: this.renderRow,
        rechEnd: this.handleLoadMore,
        ListFoot: this.renderFooter
      }}>
        {/* <FlatList
          numColumns={2}
          style={{ marginTop: 20, }}
          data={this.state.data}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={10}
          ListFooterComponent={this.renderFooter}
        /> */}
        <ChildContext />
      </Context.Provider>
    );
  }
};



export default App;


// class Child extends React.PureComponent {

//   render() {
//     return (
//       // <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//         <Context.Consumer>

//           {data => //console.log(data.renItem)
//             <FlatList
//               numColumns={2}
//               style={{ marginTop: 20, }}
//               data={data.originalData}
//               renderItem={data.renItem}
//               keyExtractor={(item, index) => index.toString()}
//               onEndReached={data.rechEnd}
//               onEndReachedThreshold={10}
//               ListFooterComponent={data.ListFoot}
//             />
//           }
//           {/* {data => <Text style={{ fontSize: 18, textAlign: 'center', margin: 10 }}> Child Context is : </Text>} */}
//         </Context.Consumer>
//       // </View>
//     )
//   }
// }