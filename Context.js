import React, { Component } from 'react'
import { Text, View, Button,FlatList } from 'react-native'

//const Context = React.createContext('default_val')
import Context from './AuthContext'

// export default class Contextt extends Component {

//     constructor() {
//         super()
//         this.state = {
//             Contextdata: 'Initial Context value'
//         }
//     }

//     render() {
//         return (
//             <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//                 <Context.Provider value={{ val: this.state.Contextdata }}>
//                     <Text> Parent Context </Text>
//                     <Button onPress={() => this.setState({ Contextdata: 'Dynamic Data Context' })} title='Change Value' />
//                     <Child />
//                 </Context.Provider>
//             </View>
//         ) 
//     }
// }
 
export default class Child extends React.PureComponent {

    render() {
      return (
        // <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Context.Consumer>
  
            {data => //console.log(data.renItem)
              <FlatList
                numColumns={2}
                style={{ marginTop: 20, }}
                data={data.originalData}
                renderItem={data.renItem}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={data.rechEnd}
                onEndReachedThreshold={10}
                ListFooterComponent={data.ListFoot}
              />
            }
            {/* {data => <Text style={{ fontSize: 18, textAlign: 'center', margin: 10 }}> Child Context is : </Text>} */}
          </Context.Consumer>
        // </View>
      )
    }
  }

